
def write_csv(content, file_name):
    content = '{},{},{}'.format(content[2], content[1], content[0])
    with open(file_name, 'a') as f:
        f.write(content + '\n')


def write_error(content, file_name):
    content = '{}'.format(content)
    with open(file_name, 'a') as f:
        f.write(content + '\n')


