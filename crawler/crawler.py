import logging
import re
import requests

from multiprocessing import Process, Queue
from bs4 import BeautifulSoup
from crawler.csv import write_csv, write_error


logger = logging.getLogger()
handler = logging.StreamHandler()
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class Crawler:
    def __init__(self, domain, num_workers=4, max_depth=1, filename='product.csv'):
        if not domain:
            raise Exception("Domain is required")
        if not isinstance(domain, str):
            raise Exception("Domain is not instance of str")
        self.domain = domain
        self.max_depth = max_depth
        self.visited_url = set()
        self.unvisited_url = [self.domain]
        self.processes = []
        self.targets = Queue()
        self.num_workers = num_workers
        self.filename = filename

    def process_url(self, url):
        """
        Given a url , checks if is a product link and get content else get more links
        """
        try:
            html = requests.get(url).content
            page = BeautifulSoup(html, "html.parser")
            if url.endswith('/p'):
                content = self.get_content(url, page)
                self.targets.put(content)
            else:
                links = self.get_urls(page)
                self.targets.put(links)
        except requests.ConnectionError:
            self.targets.put('connection error url: '+url)

    def get_urls(self, page):
        """
        Given a html page get all links
        """
        all_links = page.find_all('a', attrs={"href": re.compile(self.domain)})
        urls = set()
        for link in all_links:
            if (link['href'] not in self.visited_url and link['href'] not in self.unvisited_url
                and len(link['href'].replace(self.domain, '').split('/')) <= self.max_depth)\
                    or link['href'].endswith('/p'):
                    urls.add(link['href'])

        return urls

    def get_content(self, url, page):
        """
        Receive an url and page, and get content about product
        :return: (ur, title, product name)
        """
        try:
            content = url, page.title.text, page.find('div', class_="productName").text
            return content
        except AttributeError:
            self.targets.put('error :' + url)

    def run(self):
        unvisited_links = self.unvisited_url
        while unvisited_links:
            for url in unvisited_links:
                if not (url not in self.visited_url and len(self.processes) < self.num_workers):
                    break
                p = Process(target=self.process_url, args=[url])
                p.start()
                self.processes.append(p)
                self.visited_url.add(url)
            while self.processes:
                worker = self.processes.pop()
                worker.join()
                msg = self.targets.get()
                if isinstance(msg, set):
                    self.unvisited_url.extend(msg)
                if isinstance(msg, tuple):
                    write_csv(msg, self.filename)
                if isinstance(msg, str):
                    file_error = self.filename.split('.')
                    file_error[0] += '_error'
                    write_error(msg, '.'.join(file_error))

                unvisited_links = set(self.unvisited_url) - self.visited_url
