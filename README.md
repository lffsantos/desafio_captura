# Catch

Crawler que visita o site epocacosmeticos.com.br e salva um arquivo .csv com o 
nome do produto, o título e a url de cada página de produto encontrada.

# Pré requisitos e Instalação

- First of all, catch requires Python 3.5 or greater.


>python --version


- Create a virtual env (optional)

```
virtualenv -p python3 .venv
source .venv/bin/activate
```

- Install requirements


>pip install -r requirements.txt



## Executar


>python run.py 

```
usage: run.py [-h] [-w WORKERS] [-p MAX_DEPTH] [-f FILENAME]
```

## Tests


>py.test