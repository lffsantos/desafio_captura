import os
import pytest
import requests

from bs4 import BeautifulSoup
from crawler.crawler import Crawler


@pytest.fixture
def crawler():
    c = Crawler('http://www.epocacosmeticos.com.br/', 1, 1, 'file.txt')
    yield c


@pytest.mark.parametrize('url', ['http://www.epocacosmeticos.com.br/'])
def test_get_urls(crawler, url):
    html = requests.get(url).content
    page = BeautifulSoup(html, "html.parser")
    links = crawler.get_urls(page)
    assert len(set(links)) == len(links)
    for l in links:
        assert l.startswith(url)


@pytest.mark.parametrize('url, title, product_name', [
    (
        'http://www.epocacosmeticos.com.br/ecologie-fios-anticaspa-ecologie-shampoo-anticaspa/p',
        'Shampoo Anticaspa Ecologie Fios Anticaspa - Época Cosméticos',
        'Ecologie Fios Anticaspa  - Shampoo Anticaspa - 275ml'
    ),
    (
        'http://www.epocacosmeticos.com.br/redken-for-men-maneuever-cera-modeladora/p',
        'Cera Modeladora Redken For Men Maneuver - Época Cosméticos',
        'Redken For Men Maneuver  - Cera Modeladora - 100ml'
    )
])
def test_get_content(crawler, url, title, product_name):
    html = requests.get(url).content
    page = BeautifulSoup(html, "html.parser")
    content = crawler.get_content(url, page)
    assert content == (url, title, product_name)


@pytest.mark.parametrize('payload', [
    {
        'url': 'http://www.epocacosmeticos.com.br/ecologie-fios-anticaspa-ecologie-shampoo-anticaspa/p',
        'expected': (
            'http://www.epocacosmeticos.com.br/ecologie-fios-anticaspa-ecologie-shampoo-anticaspa/p',
            'Shampoo Anticaspa Ecologie Fios Anticaspa - Época Cosméticos',
            'Ecologie Fios Anticaspa  - Shampoo Anticaspa - 275ml'
        )

    },
    {
        'url': 'http://www.epocacosmeticos.com.br/',
    }
])
def test_process_url(crawler, payload):
    crawler.process_url(payload['url'])
    if isinstance(payload.get('expected'), tuple):
        assert crawler.targets.get() == payload['expected']
    else:
        assert isinstance(crawler.targets.get(), set)


def test_run(crawler):
    crawler.max_depth = 0
    crawler.filename = 'test.csv'
    crawler.run()
    count_rows = 0
    with open('test.csv', 'r') as file:
        for _ in file:
            count_rows += 1
    assert len(crawler.visited_url)-1 == count_rows
    os.remove(crawler.filename)


