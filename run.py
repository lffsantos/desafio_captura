import argparse
import logging
import time
from crawler.crawler import Crawler

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def run(domain, num_workers, max_depth, filename):
    start_time = time.time()
    c = Crawler(domain, num_workers, max_depth, filename)
    c.run()
    end = time.time()
    logger.debug('Time : {:.2f} seconds'.format(end-start_time))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--workers', type=int, default=4, help='number of workers')
    parser.add_argument('-p', '--max_depth', type=int, default=1, help='max depth')
    parser.add_argument('-f', '--filename', type=str, default='products.csv', help='filename')

    args = parser.parse_args()
    domain = 'http://www.epocacosmeticos.com.br/'
    run(domain, args.workers, args.max_depth, args.filename)
